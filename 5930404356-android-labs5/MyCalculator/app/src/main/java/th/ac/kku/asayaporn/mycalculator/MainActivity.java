package th.ac.kku.asayaporn.mycalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.regex.Pattern;

import static java.lang.Float.parseFloat;

public class MainActivity extends AppCompatActivity {

    Switch switch1 = null;
    Button button = null;
    RadioGroup radiog = null;
    EditText nume1 = null;
    EditText nume2 = null;
    TextView result = null;
    RadioButton radioplus = null;
    float num1 = 0;
    float num2 = 0;
    boolean checktoast = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get width and height
        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        String w=new String(""+width);
        String h=new String(""+height);
        showToast("Width = "+w+", Height = "+h);


        switch1 = (Switch) findViewById(R.id.switch1);
        button = (Button) findViewById(R.id.calculator);
        radiog = (RadioGroup) findViewById(R.id.radiog);
        nume1 = (EditText) findViewById(R.id.num1);
        nume2 = (EditText) findViewById(R.id.num2);
        result = (TextView) findViewById(R.id.result);
        radioplus = (RadioButton) findViewById(R.id.plus);
        radioplus.setChecked(true);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculate(radiog.getCheckedRadioButtonId());
            }
        });

        radiog.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                calculate(checkedId);
            }
        });



        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    switch1.setText("ON");
                } else {
                    switch1.setText("OFF");
                }
            }
        });
    }

    private void calculate(int checkedRadioButtonId) {

        //computation time
        double start = System.currentTimeMillis() * 0.001;
        Log.d("Calculation", "computation time = " + String.valueOf(new DecimalFormat("####.###").format(0)));
        acceptNumbers();
        float res = 0;
        if (checkedRadioButtonId == R.id.plus) {
            res = num1 + num2;
        } else if (checkedRadioButtonId == R.id.minus) {
            res = num1 - num2;
        } else if (checkedRadioButtonId == R.id.times) {
            res = num1 * num2;
        } else if (checkedRadioButtonId == R.id.divide) {
            if (num2 != 0) {
                res = num1 / num2;
            } else {
                if (checktoast == false) {
                    showToast("Please divide by a non-zero number");
                }
                return;
            }
        }
        result.setText("= " + String.valueOf(res));
        // end computation time
        double runTime = System.currentTimeMillis() * 0.001 - start;
        Log.d("Calculation", "computation time = " + String.valueOf(new DecimalFormat("####.###").format(runTime)));
    }

    private void acceptNumbers() {
        checktoast = false;
        // check regular expression !!
        if (Pattern.matches("[+-]?((\\d+\\.?\\d*)|(\\.\\d+))", String.valueOf(nume1.getText())) == false
                && Pattern.matches("[+-]?((\\d+\\.?\\d*)|(\\.\\d+))", String.valueOf(nume2.getText())) == false) {
            showToast("Please enter only a number");
            nume1.setText("");
            nume2.setText("");
            num1 = 0;
            num2 = 0;
        } else if (Pattern.matches("[+-]?((\\d+\\.?\\d*)|(\\.\\d+))", String.valueOf(nume2.getText())) == false) {
            showToast("Please enter only a number");
            nume2.setText("");
            num1 = (float) parseFloat(String.valueOf(nume1.getText()));
            num2 = 0;
            checktoast = true;
        } else if (Pattern.matches("[+-]?((\\d+\\.?\\d*)|(\\.\\d+))", String.valueOf(nume1.getText())) == false) {
            showToast("Please enter only a number");
            nume1.setText("");
            num1 = 0;
            num2 = (float) parseFloat(String.valueOf(nume2.getText()));
        } else {
            num1 = (float) parseFloat(String.valueOf(nume1.getText()));
            num2 = (float) parseFloat(String.valueOf(nume2.getText()));
        }
    }

    private void showToast(String s) {
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
    }

}
