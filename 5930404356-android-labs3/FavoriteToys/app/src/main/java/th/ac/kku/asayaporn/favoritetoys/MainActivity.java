package th.ac.kku.asayaporn.favoritetoys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        TextView tvGreen = (TextView) findViewById(R.id.green);
        tvGreen.setText("Green");
        tvGreen.setTextSize(30);
        tvGreen.setGravity(Gravity.CENTER);
    }
}
