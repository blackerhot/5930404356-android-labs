package th.ac.kku.asayaporn.pichet;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;


public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent intent = getIntent();
        TextView textView = (TextView) findViewById(R.id.textView3);
        textView.setText(String.valueOf(intent.getStringExtra("Name"))+" "+getString(R.string.show)+" "
                +intent.getStringExtra("Phone"));
    }
}
