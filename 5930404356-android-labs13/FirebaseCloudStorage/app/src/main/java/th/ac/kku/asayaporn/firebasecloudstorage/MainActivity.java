package th.ac.kku.asayaporn.firebasecloudstorage;


import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;


public class MainActivity extends AppCompatActivity {

    private StorageReference mStorageRef;
    TextView textViewResult;
    private static final int RESULT_LOAD_IMAGE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewResult  = (TextView) findViewById(R.id.tv_result);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        uploadFile("/sdcard/DCIM/P61116-183555.jpg");
        downloadPic();
    }

    public void  downloadPic(){
        try {
            StorageReference riversRef;
            riversRef = mStorageRef.child("images/P61116-183555.jpg");
            final File localFile = File.createTempFile("images", "jpg");
            riversRef.getFile(localFile)
                    .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(MainActivity.this,
                                    "downloaded file is  " + localFile.getAbsolutePath(),
                                    Toast.LENGTH_LONG).show();
                            textViewResult.append("downloaded file is  " + localFile.getAbsolutePath());

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(MainActivity.this,
                            "Fail in download file " + localFile.getName(),
                            Toast.LENGTH_LONG).show();
                    textViewResult.append(" Fail in download file " + localFile.getName());
                }
            });
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };



    private void uploadFile(final String picturePath) {
        Uri file = Uri.fromFile(new File(picturePath));
        StorageReference riversRef = mStorageRef.child("images/" + file.getLastPathSegment());
        UploadTask uploadTask = riversRef.putFile(file);
        Toast.makeText(MainActivity.this, "file is " + file.toString(),
                Toast.LENGTH_LONG).show();

        textViewResult.append("file is " + file.toString() + " last segment " + file.getLastPathSegment());
        uploadTask
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Get a URL to the uploaded content
                        Uri downloadUrl = Uri.parse(taskSnapshot.getMetadata().getReference().getDownloadUrl().toString());
                        Toast.makeText(MainActivity.this,
                                "Download url is " + downloadUrl.toString(),
                                Toast.LENGTH_LONG).show();
                        textViewResult.append("Download url is " + downloadUrl.toString());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(MainActivity.this,
                                "Fail", Toast.LENGTH_LONG).show();
                        textViewResult.append("\nFail because " +exception.toString());
                    }
                });

    }
}
