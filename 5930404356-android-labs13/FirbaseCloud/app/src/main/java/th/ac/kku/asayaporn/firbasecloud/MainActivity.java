package th.ac.kku.asayaporn.firbasecloud;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private StorageReference mStorageRef;

   private static final int RESULT_LOAD_IMAGE = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView textViewResult = (TextView) findViewById(R.id.tv_result);
        mStorageRef = FirebaseStorage.getInstance().getReference();


        Button loadpic = (Button) findViewById(R.id.butLoad);

        loadpic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               Intent i = new Intent(
                       Intent.ACTION_PICK,
                       MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i,RESULT_LOAD_IMAGE);




            }
        });



    }

    public void  downloadPic(){
        try {
            StorageReference riversRef;
            riversRef = mStorageRef.child("images/P81101-222639.jpg");
            final File localFile = File.createTempFile("images", "jpg");
            riversRef.getFile(localFile)
                    .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(MainActivity.this,
                                    "downloaded file is  " + localFile.getAbsolutePath(),
                                    Toast.LENGTH_LONG).show();
                           // textViewResult.append("downloaded file is  " + localFile.getAbsolutePath());
                            ImageView img = (ImageView) findViewById(R.id.img);
                            Bitmap bmImg = BitmapFactory.decodeFile(localFile.getAbsolutePath());
                            img.setImageBitmap(bmImg);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(MainActivity.this,
                            "Fail in download file " + localFile.getName(),
                            Toast.LENGTH_LONG).show();
                 //   textViewResult.append("Fail in download file " + localFile.getName());
                }
            });
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);

            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            final String picturePath = cursor.getString(columnIndex);
            cursor.close();
            TextView tvChosenFile = (TextView) findViewById(R.id.tvloadpic);
            tvChosenFile.setText(picturePath);
            verifyStoragePermissions(MainActivity.this);

            ImageView imageView = (ImageView) findViewById(R.id.img);
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            Button upload = (Button) findViewById(R.id.butUpload);
            upload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    uploadFile(picturePath);
                }
            });

        }
    }

    private void uploadFile(final String picturePath) {

                Uri file = Uri.fromFile(new File(picturePath));

                StorageReference riversRef = mStorageRef.child("images/" + file.getLastPathSegment());

                UploadTask uploadTask = riversRef.putFile(file);

                Toast.makeText(MainActivity.this, "file is " + file.toString(),
                        Toast.LENGTH_LONG).show();
                Log.d("Pichet", "file is " + file.toString());
              //  textViewResult.append("file is " + file.toString() + "last segment " +file.getLastPathSegment());
                uploadTask
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                // Get a URL to the uploaded content
                                Uri downloadUrl = Uri.parse(taskSnapshot.getMetadata().getReference().getDownloadUrl().toString());
                                Toast.makeText(MainActivity.this,
                                        "Download url is " + downloadUrl.toString(),
                                        Toast.LENGTH_LONG).show();
                                TextView tvupload = (TextView) findViewById(R.id.tvupload);
                                tvupload.setText(picturePath);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Toast.makeText(MainActivity.this,
                                        "Fail", Toast.LENGTH_LONG).show();
                              //  textViewResult.append("\nFail because " +exception.toString());
                            }
                        });

    }
}
