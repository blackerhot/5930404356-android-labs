package th.ac.kku.asayaporn.firebasecloudmessaging;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        TextView tv= (TextView) findViewById(R.id.textView1);
        tv.setText("Welcome to Result Activity");
    }
}
