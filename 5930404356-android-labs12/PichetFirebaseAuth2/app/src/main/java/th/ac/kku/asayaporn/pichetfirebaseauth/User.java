package th.ac.kku.asayaporn.pichetfirebaseauth;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class User {

    public String username;
    public String email;

    public User(){

    }

    public User(String username,String email){
        this.email=email;
        this.username=username;
    }

    @Exclude
    public Map<String,Object> toMap(){
        HashMap<String,Object> result = new HashMap<>();
        result.put("username",username);
        result.put("email",email);
        return  result;
    }

}
