package th.ac.kku.asayaporn.pichetfirebaseauth;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class SecondActivity extends AppCompatActivity {
    FirebaseDatabase database;
    DatabaseReference myRef;
    String TAG="DAAAAATAABASEE";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Button butupdate = (Button) findViewById(R.id.butUpdate);
        Button butadd = (Button) findViewById(R.id.butAdduser);
        final TextView tvUser1 = (TextView) findViewById(R.id.tvUser1);
        final TextView tvUser2 = (TextView) findViewById(R.id.tvUser2);
        final TextView tvEmail1 = (TextView) findViewById(R.id.tvEmail1);
        final TextView tvEmail2= (TextView) findViewById(R.id.tvEmail2);
        final TextView tvUser = (TextView) findViewById(R.id.tvUser);
        final TextView tvEmail = (TextView) findViewById(R.id.tvEmail);
        TextView tv = (TextView) findViewById(R.id.tvStatus);
        Button butlogout = (Button) findViewById(R.id.butLogout);
        Bundle bundle = getIntent().getExtras();
        tv.setText(bundle.getString("email")+" is logged in successfully");
        butlogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                onBackPressed();

            }
        });
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("message");
        //ข้อที่ 2
        //writeDB();
       // readDB();

        //ข้อที่ 3 4 5
        writeNewUser("blackerhot","pichet","pichet_pie@kkumail.com");
        readNewUser();

        butadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                writeNewUser("userID1",tvUser1.getText()+"",tvEmail1.getText()+"");
                writeNewUser("userID2",tvUser2.getText()+"",tvEmail2.getText()+"");
            }
        });

        butupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                writeUpdateUser(tvUser.getText().toString(), String.valueOf(tvEmail.getText()));
            }
        });

    }

    private void writeUpdateUser(String userId, String email) {
        Map<String,Object> childUpdates = new HashMap<>();

        childUpdates.put("/users/"+userId+"/email",email);
        myRef.updateChildren(childUpdates);
    }

    private void readNewUser() {
        myRef.child("users").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                User user = dataSnapshot.getValue(User.class);
                Toast.makeText(SecondActivity.this,
                        "Username is "+user.toMap().get("username")
                        ,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                User user = dataSnapshot.getValue(User.class);
                Toast.makeText(SecondActivity.this,
                        "Email is "+user.toMap().get("email"),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void writeNewUser(String userId,String name,String email) {
        String key = myRef.child("users").push().getKey();
        User user = new User(name,email);
        Map<String,Object> userValues = user.toMap();

        Map<String,Object> childUpdates = new HashMap<>();
        childUpdates.put("/users/"+userId,userValues);
        myRef.updateChildren(childUpdates);
    }

    private void readDB() {
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Toast.makeText(SecondActivity.this,"Value is: " +value,Toast.LENGTH_SHORT).show();
                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value

                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    private void writeDB() {

        myRef.setValue("Hello, World!");
    }
}
