package th.ac.kku.asayaporn.nationsapicaller;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    EditText url;
    TextView responseView;
    Button search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        responseView =
                (TextView) findViewById(R.id.responseView);
        url = (EditText) findViewById(R.id.urlText);
        search = (Button) findViewById(R.id.queryButton);
        url.setText("https://fb.kku.ac.th/krunapon/json/nations.json");
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new RetrieveFeedTask().execute();

            }
        });


    }

    ProgressDialog progressBar;

    class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private Exception exception;

        protected void onPreExecute() {
            progressBar = new ProgressDialog(MainActivity.this);
            progressBar.setMessage("Please wait");
            progressBar.setCancelable(false);
            progressBar.show();
            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {
            String urlinside = url.getText().toString();
            // Do some validation here

            try {
                URL url = new URL(urlinside);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (progressBar.isShowing()) {
                progressBar.dismiss();
            }
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }


            JSONObject obj = null;
            try {
                obj = new JSONObject(response);

                if (url.getText().toString().equals("https://fb.kku.ac.th/krunapon/json/nations.json")) {
                    Log.i("INFO", response);
                    JSONArray nations = (JSONArray) obj.get("nations");
                    String str = "";
                    for (int i = 0; i < nations.length(); i++) {
                        JSONObject nationsinside = (JSONObject) nations.getJSONObject(i);
                        str += nationsinside.getString("name") + " is in "
                                + nationsinside.getString("location") + "\n";
                    }
                    responseView.setText(str);
                } else if (url.getText().toString().equals("https://fb.kku.ac.th/krunapon/json/iptest.json")) {
                    Log.i("INFO", response);
                    responseView.setText(obj.get("ip").toString());
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


}
