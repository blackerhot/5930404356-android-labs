package th.ac.kku.asayaporn.intent;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 10;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void okButtonClicked(View view) {
        EditText name = (EditText) findViewById (R.id.editText);
        // To do 1. create new intent
        // To do 2. put extra value with the intent
        // To do 3. use startActivityForResult with REQUEST_CODE

        Intent data = new Intent(this,SecondActivity.class);
        data.putExtra("name",name.getText().toString());
        startActivityForResult(data ,REQUEST_CODE);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            // Todo 1. get data from ResultActivity
            // Todo 2. set the value of TextView with the received data

            textView =(TextView)findViewById(R.id.result);
            textView.setText(String.valueOf(data.getExtras().getString("name")));


        }
    }





}
