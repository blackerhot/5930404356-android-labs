package th.ac.kku.asayaporn.intent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);
        setIntentData();
    }

    public void setIntentData() {
        Bundle extras = getIntent().getExtras();
        String inputString = extras.getString("name");
        TextView view = (TextView) findViewById(R.id.textView);
        view.setText(inputString);
    }

    @Override
    public void finish() {
        // TODO 1 create new Intent
        // TODO 2 read the data of the EditText field
        // with the id returnValue
        EditText editText = (EditText) findViewById(R.id.returnValue);
        // TODO 3 put the text from EditText
        // as String extra into the intent

        Intent intent = new Intent();
        intent.putExtra("name",editText.getText().toString());

        // return the Intent to the application
        setResult(RESULT_OK, intent);
        super.finish();

    }


}
