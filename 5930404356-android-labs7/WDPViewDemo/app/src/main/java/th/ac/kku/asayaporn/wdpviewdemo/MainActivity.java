package th.ac.kku.asayaporn.wdpviewdemo;

import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText editext1;
    EditText editext2;
    Button button;
    TextView textview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editext1 = (EditText) findViewById(R.id.edit1);
        editext2 = (EditText) findViewById(R.id.edit2);
        button = (Button) findViewById(R.id.button);
        textview = (TextView) findViewById(R.id.result);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    Float num1 = (Float) Float.parseFloat(String.valueOf(editext1.getText()));
                    Float num2 = (Float) Float.parseFloat(String.valueOf(editext2.getText()));
                    Float result = num1 + num2;
                    textview.setText(String.valueOf(result));

                } catch (Exception e) {
                    Toast.makeText(MainActivity.this
                            , "Please Enter Again", Toast.LENGTH_SHORT).show();

                }


            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("num1", editext1.getText().toString());
        outState.putString("num2", editext2.getText().toString());
        outState.putString("result", textview.getText().toString());

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        editext1.setText(savedInstanceState.getString("num1"));
        editext2.setText(savedInstanceState.getString("num2"));
        textview.setText(savedInstanceState.getString("result"));
    }
}
