package th.ac.kku.asayaporn.currentgeocoder;

import android.Manifest;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    public static final String TAG = "MAPPY";
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private final static int CONNEC_FIAL_REQUEST = 9000;
    EditText editlat, editlng, editaddress = null;
    RadioGroup rdg = null;
    Button btn = null;
    TextView tvresult = null;
    RadioButton rdiolat = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this).addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        rdiolat = (RadioButton) findViewById(R.id.radioLatLng);
        editaddress = (EditText) findViewById(R.id.editAddress);
        editlat = (EditText) findViewById(R.id.editLat);
        editlng = (EditText) findViewById(R.id.editLong);
        rdg = (RadioGroup) findViewById(R.id.radiog);
        btn = (Button) findViewById(R.id.btnFetch);
        tvresult = (TextView) findViewById(R.id.result);

        rdiolat.setChecked(true);
        rdg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.radioLatLng) {
                    editaddress.setEnabled(false);
                    editlat.setEnabled(true);
                    editlng.setEnabled(true);


                } else if (i == R.id.radioAddress) {
                    editlat.setEnabled(false);
                    editlng.setEnabled(false);
                    editaddress.setEnabled(true);

                }
            }
        });


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
                String result = null;

                if (!rdiolat.isChecked()) {

                    try {
                        List<Address> list = geocoder.getFromLocationName(
                                String.valueOf(editaddress.getText()), 1);
                        if (list != null && list.size() > 0) {
                            Address address = list.get(0);
                            result = "Lat: " + address.getLatitude() + "\nLng:" + address.getLongitude();
                            tvresult.setText(result);
                        }
                        // sending back first address line and locality


                    } catch (IOException e) {
                        Log.e(TAG, "Impossible to connect to Geocoder", e);
                    }
                } else {


                    try {

                        List<Address> list = geocoder.getFromLocation(
                                Double.parseDouble(String.valueOf(editlat.getText())),
                                Double.parseDouble(String.valueOf(editlng.getText())), 1);

                        if (list != null && list.size() > 0) {
                            Address address = list.get(0);
                            result = address.getAddressLine(0) + ". " + address.getLocality();
                            tvresult.setText(result);
                        }


                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
        });


    }


    @Override
    public void onLocationChanged(Location location) {

        String result = "";
        Geocoder geocoder = new Geocoder(MainActivity.this, Locale.getDefault());
        editlat.setText("" + location.getLatitude());
        editlng.setText("" + location.getLongitude());


        try {

            List<Address> list = geocoder.getFromLocation(
                    Double.parseDouble(String.valueOf(location.getLatitude())),
                    Double.parseDouble(String.valueOf(location.getLongitude())), 1);

            if (list != null && list.size() > 0) {
                Address address = list.get(0);
                result = address.getAddressLine(0) + ". " + address.getLocality();
                editaddress.setText(result);
            }


        } catch (IOException e) {
            e.printStackTrace();
        }



    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10);
        mLocationRequest.setFastestInterval(10);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {

            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, CONNEC_FIAL_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " +
                    connectionResult.getErrorCode());
        }
    }
}
