package th.ac.kku.asayaporn.simplegeocode2;

import android.Manifest;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity {


    public static final String TAG = "MAPPY";

    private final static int CONNEC_FIAL_REQUEST = 9000;
    EditText editlat, editlng, editaddress = null;
    RadioGroup rdg = null;
    Button btn = null;
    TextView tvresult = null;
    RadioButton rdiolat =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo);

        rdiolat= (RadioButton) findViewById(R.id.radioLatLng);
        editaddress = (EditText) findViewById(R.id.editAddress);
        editlat = (EditText) findViewById(R.id.editLat);
        editlng = (EditText) findViewById(R.id.editLong);
        rdg = (RadioGroup) findViewById(R.id.radiog);
        btn = (Button) findViewById(R.id.btnFetch);
        tvresult = (TextView) findViewById(R.id.result);

        rdiolat.setChecked(true);
        rdg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.radioLatLng) {
                    editaddress.setEnabled(false);
                    editlat.setEnabled(true);
                    editlng.setEnabled(true);


                } else if (i == R.id.radioAddress) {
                    editlat.setEnabled(false);
                    editlng.setEnabled(false);
                    editaddress.setEnabled(true);

                }
            }
        });




        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Geocoder geocoder = new Geocoder(MapsActivity.this, Locale.getDefault());;
                String result = null;

                if(!rdiolat.isChecked()){



                    try {
                        List<Address> list = geocoder.getFromLocationName(
                                String.valueOf(editaddress.getText()),1);
                        if(list!=null && list.size()>0) {
                            Address address = list.get(0);
                            result = "Lat: "+address.getLatitude() + "\nLng:" + address.getLongitude();
                            tvresult.setText(result);
                        }
                            // sending back first address line and locality


                    } catch (IOException e) {
                        Log.e(TAG, "Impossible to connect to Geocoder", e);
                    }
                }else{


                    try {
                        StringBuilder sb = new StringBuilder();
                        List<Address> list = geocoder.getFromLocation(
                                Double.parseDouble(String.valueOf(editlat.getText())),
                               Double.parseDouble(String.valueOf(editlng.getText())), 1);

                        if(list!=null && list.size()>0){
                            Address address = list.get(0);
                            result = address.getAddressLine(0)+". "+address.getLocality();
                            tvresult.setText(result);
                        }






                } catch (IOException e) {
                        e.printStackTrace();
                    }

                }}
        });



    }








}
